# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/ascii-patrol/issues
# Bug-Submit: https://github.com/<user>/ascii-patrol/issues/new
# Changelog: https://github.com/<user>/ascii-patrol/blob/master/CHANGES
# Documentation: https://github.com/<user>/ascii-patrol/wiki
# Repository-Browse: https://github.com/<user>/ascii-patrol
# Repository: https://github.com/<user>/ascii-patrol.git
